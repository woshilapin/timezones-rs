#![doc = include_str!("../README.md")]

extern crate phf;

#[cfg(feature = "serde")]
mod serde;

// mod directory;
mod timezones;

// pub use directory::*;
pub use timezones::ParseError;
pub use timezones::Tz;
pub use timezones::TZ_VARIANTS;

#[cfg(test)]
mod tests {
    use super::Europe::London;
    use super::Tz;

    #[test]
    fn test_get_name() {
        assert_eq!(London.name(), "Europe/London");
        assert_eq!(Tz::Africa__Abidjan.name(), "Africa/Abidjan");
        assert_eq!(Tz::UTC.name(), "UTC");
        assert_eq!(Tz::Zulu.name(), "Zulu");
    }

    #[test]
    fn test_display() {
        assert_eq!(format!("{}", London), "Europe/London");
        assert_eq!(format!("{}", Tz::Africa__Abidjan), "Africa/Abidjan");
        assert_eq!(format!("{}", Tz::UTC), "UTC");
        assert_eq!(format!("{}", Tz::Zulu), "Zulu");
    }

    #[test]
    fn test_impl_hash() {
        #[allow(dead_code)]
        #[derive(Hash)]
        struct Foo(Tz);
    }
}
